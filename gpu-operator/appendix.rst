.. license-header
  SPDX-FileCopyrightText: Copyright (c) 2023 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
  SPDX-License-Identifier: Apache-2.0

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

.. Date: May 11 2021
.. Author: pramarao

.. _operator-appendix:

#######################
Advanced Configurations
#######################

.. toctree::
   :titlesonly:

   install-gpu-operator-proxy.rst

   install-gpu-operator-air-gapped.rst

   install-gpu-operator-outdated-kernels.rst

   custom-driver-params.rst

   precompiled-drivers.rst

   Container Device Interface Support <cdi.rst>

   GPU Operator with Amazon EKS <amazon-eks.rst>

   GPU Operator with Azure AKS <microsoft-aks.rst>

   GPU Operator with Google GKE <google-gke.rst>
