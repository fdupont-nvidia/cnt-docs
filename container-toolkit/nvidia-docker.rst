.. Date: December 18 2020
.. Author: pramarao

Getting Started
===============

For installing Docker CE, follow the official `instructions <https://docs.docker.com/engine/install/>`_ for your supported Linux distribution.
For convenience, the documentation below includes instructions on installing Docker for various Linux distributions.

.. Ubuntu instructions

.. include:: distro/ubuntu.rst

.. CentOS 8 instructions

.. include:: distro/centos8.rst

.. RHEL 7 instructions

.. include:: distro/rhel7.rst

.. SUSE 15 instructions

.. include:: distro/suse15.rst

.. Amazon Linux instructions

.. include:: distro/amazon-linux.rst
